#!/usr/bin/env python3

import numpy as np
from numpy.linalg import matrix_power


def adjacency_matrix(edges, n, *, symmetric = False):
    A = np.zeros( (n,n), dtype=np.int64 )
    for i in range(n):
        for j in range(n):
            if (i,j) in edges:
                A[i][j] = 1
                if symmetric:
                    A[j][i] = 1
    return A

def adjacency_matrix2(edges, n, *, symmetric = False):
    A = np.zeros( (n,n), dtype=np.int64 )
    for i,j in edges:
        A[i][j] = 1
        if symmetric:
            A[j][i] = 1
    return A

def graph_set(M):
    n,m = M.shape
    if n != m:
        raise ValueError("Not a square matrix.")
    g = []
    for i in range(n):
        for j in range(n):
            if M[i][j]:
                g.append( (i,j) )
    return g

def nb_paths(G, n,i,j,r, *, symmetric=False):
    M = adjacency_matrix(G, n, symmetric=symmetric)
    return matrix_power(M, r)[i][j]

def nb_paths_leq(G, n,i,j,r, *, symmetric=False):
    res = 0
    for l in range(r+1):
        res += nb_paths(G,n,i,j,l, symmetric=symmetric)
    return res

if __name__ == '__main__':
    g = [ (0, 1), (0, 2), (1, 2), (1,3) , (2,3) ]
    print('a1')
    print(adjacency_matrix(g,4))
    print(adjacency_matrix(g,4, symmetric = True))
    print('a2')
    print(adjacency_matrix2(g,4))
    print(adjacency_matrix2(g,4, symmetric = True))

    m = adjacency_matrix(g,4, symmetric = True)
    print(graph_set(m))
    print(nb_paths(g,4,1,2,1, symmetric=True))
    print(nb_paths_leq(g,4,1,2,3, symmetric=True))
