#!/usr/bin/env python3

import sqlite3
from cityInfoClass import City

# Rappel: structure de la table:
# CREATE TABLE cities(id integer not null primary key autoincrement,
#                       name TEXT, pop INT, area DECIMAL(5,2),
#                       gent TEXT);
# Constructeur de City:
# class City:
#    def __init__(self,name,pop=None,area=None,gent=None):


with sqlite3.connect('cities.sqlite3') as c:
    curs = c.cursor()
    curs.execute('SELECT name,pop,area,gent FROM cities')
    myCities = [ City(*record) for record in curs ]

for city in myCities:
    city.showInfo()
