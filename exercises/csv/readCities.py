#!/usr/bin/env python3

import csv

# Name, population, area, gentility
# "Lyon",513275,47.87,"Lyonnais"
fname = 'cities.csv'

cities = []
dict_cities = {}
with open(fname) as fd:
    rdr = csv.reader(fd)
    for name,pop,area,gent in rdr:
        # build a list of tuples
        cities.append( (name,int(pop),float(area),gent) )
        # build a dictionary, values are tuples
        dict_cities[name] = (int(pop),float(area),gent)

from pprint import pprint
pprint(cities)
pprint(dict_cities)

# en compréhension

with open(fname) as fd:
    rdr = csv.reader(fd)
    cities = [ (name,int(pop),float(area),gent) \
               for name,pop,area,gent in rdr ]

pprint(cities)

# Dictionnaire en compréhension

with open(fname) as fd:
    rdr = csv.reader(fd)
    dict_cities = { name:(int(pop),float(area),gent) \
                    for name,pop,area,gent in rdr }

pprint(dict_cities)

dict_cities['Une "ville" au nom, bizarre'] = (1,1,'spam')

def dict_to_csv(cities_dict, fname):
    with open(fname,'w') as fd:
        wrt = csv.writer(fd,delimiter=',',quotechar='"')
        for name,(pop,area,gent) in cities_dict.items():
            wrt.writerow( (name,pop,area,gent) )

dict_to_csv(dict_cities,'test_output.csv')

