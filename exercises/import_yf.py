#!/usr/bin/env python3

import datetime as dt
import yfinance as yf
import pandas as pd

ticker_list = {'INTC': 'Intel',
               'MSFT': 'Microsoft',
               'IBM': 'IBM',
               'BHP': 'BHP',
               'TM': 'Toyota',
               'AAPL': 'Apple',
               'AMZN': 'Amazon',
               'BA': 'Boeing',
               'QCOM': 'Qualcomm',
               'KO': 'Coca-Cola',
               'GOOG': 'Google',
               'PTR': 'PetroChina'}

def read_data(ticker_list,
          start=dt.datetime(2020, 1, 2),
          end=dt.datetime(2020, 12, 31)):
    """
    This function reads in closing price data from Yahoo
    for each tick in the ticker_list.
    """
    ticker = pd.DataFrame()

    for tick in ticker_list:
        stock = yf.Ticker(tick) # download from yf
        prices = stock.history(start=start, end=end)
        closing_prices = prices['Close']
        closing_prices.to_csv('data/{}.csv'.format(tick))
        ticker[tick] = closing_prices

    return ticker

ticker = read_data(ticker_list)

from pprint import pprint

print(ticker.head())
